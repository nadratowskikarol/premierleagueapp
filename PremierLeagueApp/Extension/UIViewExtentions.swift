import UIKit
import SnapKit

extension UIView {

    func addSubviews(_ views: UIView...) {
        views.forEach {
            self.addSubview($0)
        }
    }

    func fillParent() {
        self.snp.makeConstraints {
            make in
            make.edges.equalToSuperview()
        }
    }

}