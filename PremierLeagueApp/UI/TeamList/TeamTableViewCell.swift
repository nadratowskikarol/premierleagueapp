import UIKit
import Kingfisher

class TeamTableViewCell: UITableViewCell {
    static let reusableIdentifier = "TeamTableViewCell"

    private var logo: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 18, weight: .semibold)
        return label
    }()

    private var positionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15, weight: .regular)
        label.text = ""
        return label
    }()

    private var pointsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15, weight: .regular)
        label.text = ""
        return label
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    private func setupView() {
        backgroundColor = .white
        addSubviews(logo, nameLabel, positionLabel, pointsLabel)
        applyConstraints()
    }

    private func applyConstraints() {
        logo.snp.makeConstraints {
            make in
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(40)
            make.centerY.equalToSuperview()
        }

        nameLabel.snp.makeConstraints {
            make in
            make.top.equalToSuperview().inset(15)
            make.left.equalTo(logo.snp.right).offset(20)
            make.right.equalToSuperview()
        }

        positionLabel.snp.makeConstraints {
            make in
            make.top.equalTo(nameLabel.snp.bottom).offset(5)
            make.left.equalTo(nameLabel)
            make.bottom.equalToSuperview().inset(15)
        }

        pointsLabel.snp.makeConstraints {
            make in
            make.left.equalTo(positionLabel.snp.right).offset(20)
            make.centerY.equalTo(positionLabel)
        }
    }

    func configure(with teamWithStanding: TeamWithStanding) {
        let team = teamWithStanding.team
        let standing = teamWithStanding.standings
        nameLabel.text = team.name
        if let points = standing?.points {
            pointsLabel.text = "Points: \(String(points))"
        }
        if let position = standing?.rank {
            positionLabel.text = "Position: \(String(position))"
        }
        if let logoURL = URL(string: team.logo) {
            logo.kf.setImage(with: logoURL)
        }
    }
}