import Foundation
import Combine

struct TeamWithStanding {
    let team: Team
    let standings: Standing?
}
class TeamListViewModel {
    private var teamWithStandings = [TeamWithStanding]()
    private let api = Api()
    var onTeamListLoaded: (()->())? = nil
    var activityIndicatorStateChanged: ((Bool)->())? = nil
    var onTeamSelected: ((TeamWithStanding)->())? = nil

    var numberOfCells: Int {
        teamWithStandings.count
    }

    func fetchTeamList() {
        activityIndicatorStateChanged?(true)

        let standings = api.getStanding(for: "524")
        let teams = api.getTeams(for: "2")

        Publishers.Zip(teams, standings) // 3
            .sink(receiveCompletion: { [weak self] completion in
                self?.activityIndicatorStateChanged?(false)
            }, receiveValue: {
                [weak self] (teams, standings) in
                self?.activityIndicatorStateChanged?(false)
                guard let teams = teams as? TeamList, let standings = standings as? StandingList else {
                    return
                }
                self?.merge(teams: teams, with: standings)
            })
    }

    private func merge(teams: TeamList, with standings: StandingList) {
        guard let standingsArray = standings.api.standings.first else {
            return
        }
        let teamsArray = teams.api.teams

        teamsArray.forEach {
            let team = $0
            if let standing = standingsArray.first(where: {
                team.team_id == $0.team_id
            }) {
                self.teamWithStandings.append(TeamWithStanding(team: team, standings: standing))
            }
        }

        teamWithStandings.sort {
            $0.standings?.rank ?? 0 < $1.standings?.rank ?? 0
        }
        onTeamListLoaded?()
    }


    func getTeam(for index: Int) -> TeamWithStanding? {
        if index >= numberOfCells {
            return nil
        }
        return teamWithStandings[index]
    }
}