import UIKit

class TeamListViewController: UIViewController {
    let viewModel: TeamListViewModel
    let mainView: TeamListView

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: TeamListViewModel) {
        self.viewModel = viewModel
        self.mainView = TeamListView(viewModel: viewModel)
        super.init(nibName: nil, bundle: nil)
        configureView()
        self.title = "Preamier League"
    }

    private func configureView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }

    override func loadView() {
        super.loadView()
        view.addSubview(mainView)
        mainView.fillParent()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        viewModel.fetchTeamList()
    }

    private func bindViewModel() {
        viewModel.onTeamListLoaded = {
            [weak self] in
            self?.mainView.tableView.reloadData()
        }

        viewModel.activityIndicatorStateChanged = {
            [weak self] shouldShow in
            guard let view = self?.mainView else {
                return
            }
            if shouldShow {
                ActivityIndicator.shared.start()
            } else {
                ActivityIndicator.shared.stop()
            }
        }
    }
}

extension TeamListViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfCells
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TeamTableViewCell.reusableIdentifier, for: indexPath) as! TeamTableViewCell
        if let team = viewModel.getTeam(for: indexPath.row) {
            cell.configure(with: team)
        }
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let teamWithStanding = viewModel.getTeam(for: indexPath.row) else {
            return
        }
        viewModel.onTeamSelected?(teamWithStanding)
    }
}