import UIKit

class TeamListView: UIView {
    private let viewModel: TeamListViewModel

    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TeamTableViewCell.self, forCellReuseIdentifier: TeamTableViewCell.reusableIdentifier)
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .white
        return tableView
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: TeamListViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
    }

    private func setupView() {
        backgroundColor = .white
        addSubviews(tableView)
        applyConstraints()
    }

    private func applyConstraints() {
        tableView.fillParent()
    }
}

