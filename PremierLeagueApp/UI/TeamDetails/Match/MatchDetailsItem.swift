import UIKit
import Kingfisher

class MatchDetailsItem: UICollectionViewCell {
    static let reusableIdentifier = "MatchDetailsItem"

    private var homeTeamLogo: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private var homeTeamLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray.withAlphaComponent(0.6)
        label.text = "Home"
        label.font = .systemFont(ofSize: 30, weight: .semibold)
        return label
    }()

    private var matchInfoView: MatchInfoView = {
        let view = MatchInfoView()
        return view
    }()

    private var awayTeamLogo: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private var awayTeamLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray.withAlphaComponent(0.6)
        label.text = "Away"
        label.font = .systemFont(ofSize: 30, weight: .bold)
        return label
    }()


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    override func draw(_ rect: CGRect) {
        drawRectangle(rect)
    }

    private func drawRectangle(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.closePath()

        context.setFillColor(Colors.red.withAlphaComponent(0.3).cgColor)
        context.fillPath()
    }

    private func setupView() {
        backgroundColor = .white
        layer.cornerRadius = 5
        clipsToBounds = true
        addSubviews(homeTeamLogo, homeTeamLabel, matchInfoView, awayTeamLogo, awayTeamLabel)
        applyConstraints()
    }


    private func applyConstraints() {
        homeTeamLogo.snp.makeConstraints {
            make in
            make.left.top.equalToSuperview().inset(20)
            make.height.width.equalTo(70)
        }

        homeTeamLabel.snp.makeConstraints {
            make in
            make.left.equalTo(homeTeamLogo.snp.right).offset(25)
            make.top.equalTo(homeTeamLogo)
        }

        awayTeamLogo.snp.makeConstraints {
            make in
            make.right.bottom.equalToSuperview().inset(20)
            make.height.width.equalTo(70)
        }

        awayTeamLabel.snp.makeConstraints {
            make in
            make.right.equalTo(awayTeamLogo.snp.left).offset(-25)
            make.bottom.equalTo(awayTeamLogo)
        }

        matchInfoView.snp.makeConstraints {
            make in
            make.center.equalToSuperview()
            make.height.equalTo(65)
        }
    }

    func configure(with match: Match) {
        matchInfoView.configure(with: match)
        if let homeLogoURL = URL(string: match.homeTeam.logo) {
            homeTeamLogo.kf.setImage(with: homeLogoURL)
        }
        if let awayLogoURL = URL(string: match.awayTeam.logo) {
            awayTeamLogo.kf.setImage(with: awayLogoURL)
        }
    }
}
