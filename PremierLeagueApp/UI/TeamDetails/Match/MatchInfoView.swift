import UIKit

class MatchInfoView: UIView {

    private var timeLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.mainDarkColor
        label.text = ""
        label.font = .systemFont(ofSize: 18, weight: .semibold)
        return label
    }()

    private var statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.mainDarkColor
        label.text = ""
        label.font = .systemFont(ofSize: 18)
        label.isHidden = true
        return label
    }()

    private var homeTeamScoreLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.mainDarkColor
        label.text = ""
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        return label
    }()

    private var dotLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.mainDarkColor
        label.text = ":"
        label.font = .systemFont(ofSize: 18, weight: .semibold)
        return label
    }()

    private var awayTeamScoreLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.mainDarkColor
        label.text = ""
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        return label
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        super.init(frame: .zero)
        setupView()
    }

    private func setupView() {
        layer.cornerRadius = 5
        backgroundColor = Colors.mainColor
        addSubviews(timeLabel, statusLabel, homeTeamScoreLabel, dotLabel, awayTeamScoreLabel)
        applyConstraints()
    }

    private func applyConstraints() {
        timeLabel.snp.makeConstraints {
            make in
            make.top.equalToSuperview().offset(5)
            make.left.right.equalToSuperview()
        }

        statusLabel.snp.makeConstraints {
            make in
            make.bottom.equalToSuperview().inset(5)
            make.centerX.equalToSuperview()
        }

        homeTeamScoreLabel.snp.makeConstraints {
            make in
            make.bottom.equalToSuperview().inset(5)
            make.right.equalTo(dotLabel.snp.left).offset(-15)
        }

        awayTeamScoreLabel.snp.makeConstraints {
            make in
            make.bottom.equalToSuperview().inset(5)
            make.left.equalTo(dotLabel.snp.right).offset(15)
        }

        dotLabel.snp.makeConstraints {
            make in
            make.bottom.equalToSuperview().inset(5)
            make.centerX.equalToSuperview()
        }
    }

    func configure(with match: Match) {
        [homeTeamScoreLabel, dotLabel, awayTeamScoreLabel, statusLabel].forEach {
            $0.isHidden = true
        }
        timeLabel.text = DateConverter.convertDate(text: match.event_date)
        if let homeScore = match.goalsHomeTeam, let awayScore = match.goalsAwayTeam {
            homeTeamScoreLabel.isHidden = false
            homeTeamScoreLabel.text = String(homeScore)
            awayTeamScoreLabel.isHidden = false
            awayTeamScoreLabel.text = String(awayScore)
            dotLabel.isHidden = false
        } else {
            statusLabel.isHidden = false
            statusLabel.text = match.status
        }
    }

}