import UIKit
import UPCarouselFlowLayout
import Kingfisher

class TeamDetailsView: UIView {
    private let viewModel: TeamDetailsViewModel

    private var backgroundLogo: UIImageView = {
        let image = UIImage(named: "aguero")
        let imageView = UIImageView(image: image)
        imageView.alpha = 0.7
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    let container = UIView()

    private let backgroundView = UIView()

    private var currentPositionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 37, weight: .semibold)
        return label
    }()

    private var currentTextPositionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Current place"
        label.font = .systemFont(ofSize: 16)
        return label
    }()

    private var teamLogo: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private var pointsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 37, weight: .semibold)
        return label
    }()

    private var pointsTextLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Current points"
        label.font = .systemFont(ofSize: 16)
        return label
    }()

    private var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 35, weight: .semibold)
        return label
    }()

    private var stadiumIcon: UIImageView = {
        let image = UIImage(systemName: "sportscourt")
        let imageView = UIImageView(image: image)
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private var stadiumLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 17)
        return label
    }()

    var collectionView: UICollectionView = {
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: 300, height: 260)
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.register(MatchDetailsItem.self, forCellWithReuseIdentifier: MatchDetailsItem.reusableIdentifier)
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: TeamDetailsViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
        updateView()
    }

    private func updateView() {
        stadiumLabel.text = viewModel.stadiumName
        nameLabel.text = viewModel.teamName
        pointsLabel.text = viewModel.points
        currentPositionLabel.text = viewModel.rank
        if let logoURL = viewModel.logoURL {
            teamLogo.kf.setImage(with: logoURL)
        }
    }

    private func setupView() {
        backgroundColor = .white
        backgroundView.backgroundColor = Colors.mainDarkColor
        backgroundView.alpha = 0.9
        addSubviews(backgroundLogo, backgroundView, container)
        container.addSubviews(currentPositionLabel, currentTextPositionLabel, teamLogo, pointsLabel, pointsTextLabel, nameLabel, stadiumLabel, stadiumIcon, collectionView)
        applyConstraints()
    }

    private func applyConstraints() {
        backgroundView.fillParent()
        backgroundLogo.fillParent()

        container.snp.makeConstraints {
            make in
            make.centerY.equalToSuperview()
            make.width.equalToSuperview()
        }

        teamLogo.snp.makeConstraints {
            make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(20)
            make.height.width.equalTo(100)
        }

        currentPositionLabel.snp.makeConstraints {
            make in
            make.centerX.equalTo(currentTextPositionLabel)
            make.centerY.equalTo(teamLogo)
        }

        currentTextPositionLabel.snp.makeConstraints {
            make in
            make.top.equalTo(currentPositionLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().inset(15)
        }

        pointsLabel.snp.makeConstraints {
            make in
            make.centerX.equalTo(pointsTextLabel)
            make.centerY.equalTo(teamLogo)
        }

        pointsTextLabel.snp.makeConstraints {
            make in
            make.top.equalTo(currentPositionLabel.snp.bottom).offset(5)
            make.right.equalToSuperview().inset(15)
        }

        nameLabel.snp.makeConstraints {
            make in
            make.centerX.equalToSuperview()
            make.top.equalTo(teamLogo.snp.bottom).offset(45)
        }

        stadiumIcon.snp.makeConstraints {
            make in
            make.top.equalTo(nameLabel.snp.bottom)
            make.right.equalTo(stadiumLabel.snp.left).offset(-5)
        }

        stadiumLabel.snp.makeConstraints {
            make in
            make.centerY.equalTo(stadiumIcon)
            make.centerX.equalToSuperview().offset(15)
        }

        collectionView.snp.makeConstraints {
            make in
            make.top.equalTo(stadiumIcon.snp.bottom).offset(50)
            make.height.equalTo(260)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview().inset(10)
        }

    }
}

