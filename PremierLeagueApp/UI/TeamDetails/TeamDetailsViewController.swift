import UIKit

class TeamDetailsViewController: UIViewController {
    let viewModel: TeamDetailsViewModel
    let mainView: TeamDetailsView

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(viewModel: TeamDetailsViewModel) {
        self.viewModel = viewModel
        self.mainView = TeamDetailsView(viewModel: viewModel)
        super.init(nibName: nil, bundle: nil)
        configureView()
    }

    private func configureView() {
        mainView.collectionView.dataSource = self
        mainView.collectionView.delegate = self
    }

    override func loadView() {
        super.loadView()
        view.addSubview(mainView)
        mainView.fillParent()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        viewModel.fetchMatches()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
    }

    private func bindViewModel() {
        viewModel.onMatchesLoaded = {
            [weak self] in
            self?.mainView.collectionView.reloadData()
            self?.changePage()
        }

        viewModel.activityIndicatorStateChanged = {
            shouldShow in
            if shouldShow {
                ActivityIndicator.shared.start()
            } else {
                ActivityIndicator.shared.stop()
            }
        }
    }

    private func changePage() {
        let indexPath = IndexPath(row: viewModel.firstUpComingMatchRow, section: 0)
        self.mainView.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
    }
}

extension TeamDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfCells
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MatchDetailsItem.reusableIdentifier, for: indexPath) as! MatchDetailsItem
        if let match = viewModel.getMatch(for: indexPath.row) {
            cell.configure(with: match)
        }
        return cell
    }
}