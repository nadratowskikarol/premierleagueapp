import Foundation
import Combine

class TeamDetailsViewModel {
    private let teamWithStanding: TeamWithStanding
    private let api = Api()
    var matches = [Match]()
    var onMatchesLoaded: (() -> ())? = nil
    var activityIndicatorStateChanged: ((Bool) -> ())? = nil

    init(teamWithStanding: TeamWithStanding) {
        self.teamWithStanding = teamWithStanding
    }

    var numberOfCells: Int {
        matches.count
    }

    var teamName: String {
        teamWithStanding.team.name
    }

    var stadiumName: String {
        "Stadium: \(teamWithStanding.team.venue_name)"
    }

    var firstUpComingMatchRow: Int {
        guard let index = matches.firstIndex(where: {
            $0.status == "Not Started"
        }) else {
            return 0
        }

        return index
    }

    var rank: String {
        guard let rank = teamWithStanding.standings?.rank else {
            return ""
        }
        return String(rank.ordinal)
    }

    var points: String {
        guard let points = teamWithStanding.standings?.points else {
            return ""
        }
        return String(points)
    }

    var logoURL: URL? {
        return URL(string: teamWithStanding.team.logo)
    }

    func fetchMatches() {
        activityIndicatorStateChanged?(true)

        let teamId = String(teamWithStanding.team.team_id)

        let lastFixtures = api.getPlayedMatches(for: teamId)
        let nextFixtures = api.getUpComingMatches(for: teamId)

        Publishers.Zip(lastFixtures, nextFixtures) // 3
                .sink(receiveCompletion: { [weak self] completion in
                    self?.activityIndicatorStateChanged?(false)
                }, receiveValue: {
                    [weak self] (lastFixtures, nextFixtures) in
                    self?.activityIndicatorStateChanged?(false)
                    guard let lastFixtures = lastFixtures as? MatchList, let nextFixtures = nextFixtures as? MatchList else {
                        return
                    }
                    self?.matches = lastFixtures.api.fixtures.reversed() + nextFixtures.api.fixtures
                    self?.onMatchesLoaded?()
                })
    }

    func getMatch(for index: Int) -> Match? {
        if index >= numberOfCells {
            return nil
        }
        return matches[index]
    }
}


