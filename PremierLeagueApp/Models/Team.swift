import Foundation

struct Team: Codable {
    let team_id: Int
    let name: String
    let logo: String
    let venue_name: String
}
