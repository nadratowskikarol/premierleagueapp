import Foundation

struct MatchList: Codable {
    struct MatchListApi: Codable {
        let fixtures: [Match]
    }
    let api: MatchListApi
}