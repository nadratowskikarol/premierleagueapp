import Foundation

struct Standing: Codable {
    let team_id: Int
    let forme: String
    let points: Int
    let rank: Int

}