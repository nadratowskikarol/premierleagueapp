import Foundation

struct Match: Codable {
    let event_date: String
    let status: String
    let homeTeam: MatchTeam
    let awayTeam: MatchTeam
    let goalsHomeTeam: Int?
    let goalsAwayTeam: Int?
    let referee: String?
}

struct MatchTeam: Codable {
    let team_name: String
    let logo: String
}