import Foundation

struct StandingList: Codable {
    struct StandingListApi: Codable {
        let standings: [[Standing]]
    }
    let api: StandingListApi
}



