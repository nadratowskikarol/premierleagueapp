import Foundation

struct TeamList: Codable {
    struct TeamListApi: Codable {
        let teams: [Team]
    }
    let api: TeamListApi
}