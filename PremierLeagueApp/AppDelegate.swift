import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var flowController: AppFlowController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let rootNavigationController = UINavigationController()
        window?.rootViewController = rootNavigationController
        flowController = AppFlowController(navigationController: rootNavigationController)
        flowController?.runFlow()
        window?.makeKeyAndVisible()
        return true
    }




}
