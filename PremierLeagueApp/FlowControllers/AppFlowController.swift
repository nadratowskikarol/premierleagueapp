import UIKit

protocol FlowController {
    func runFlow()
}

class AppFlowController: FlowController {
    private var rootNavigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.rootNavigationController = navigationController
    }

    func runFlow() {
        showTeamListScreen()
    }

    private func showTeamListScreen() {
        let teamListVM = TeamListViewModel()
        let teamListVC = TeamListViewController(viewModel: teamListVM)
        teamListVM.onTeamSelected = {
            [weak self] teamWithStanding in
            self?.showTeamDetailsScreen(with: teamWithStanding)
        }
        self.rootNavigationController.viewControllers = [teamListVC]
    }

    private func showTeamDetailsScreen(with teamWithStanding: TeamWithStanding) {
        let teamDetailsVM = TeamDetailsViewModel(teamWithStanding: teamWithStanding)
        let teamDetailsVC = TeamDetailsViewController(viewModel: teamDetailsVM)
        rootNavigationController.pushViewController(teamDetailsVC, animated: true)
    }
}