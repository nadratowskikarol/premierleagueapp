import Foundation
import Combine

struct Client {
    struct Response<T> {
        let value: T
        let response: URLResponse
    }

    func perform<T: Decodable>(_ request: URLRequest, _ decoder: JSONDecoder = JSONDecoder()) -> AnyPublisher<Response<T>, Error> {
        var requestWithHeader = request
        requestWithHeader.setValue(NetworkingConfiguration.apiHost, forHTTPHeaderField:"x-rapidapi-host")
        requestWithHeader.setValue(NetworkingConfiguration.apiKey, forHTTPHeaderField: "x-rapidapi-key")
        return URLSession.shared
                .dataTaskPublisher(for: requestWithHeader)
                .tryMap { result -> Response<T> in
                    let value = try decoder.decode(T.self, from: result.data)
                    return Response(value: value, response: result.response)
                }
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
    }
}