import Foundation
import Combine

class Api {
    let client: Client

    init(client: Client = Client()) {
        self.client = client
    }

    // premier league - 2
    func getTeams(for leagueId: String) -> AnyPublisher<TeamList, Error> {
        let request = URLRequest(url: NetworkingConfiguration.baseURL.appendingPathComponent("teams/league/\(leagueId)"))

        return client.perform(request)
                .map(\.value)
                .eraseToAnyPublisher()
    }

    // premier league table - 52
    func getStanding(for leagueTableId: String) -> AnyPublisher<StandingList, Error> {
        let request = URLRequest(url: NetworkingConfiguration.baseURL.appendingPathComponent("leagueTable/\(leagueTableId)"))

        return client.perform(request)
                .map(\.value)
                .eraseToAnyPublisher()
    }

    func getUpComingMatches(for teamId: String) -> AnyPublisher<MatchList, Error> {
        let request = URLRequest(url: NetworkingConfiguration.baseURL.appendingPathComponent("fixtures/team/\(teamId)/next/6"))

        return client.perform(request)
                .map(\.value)
                .eraseToAnyPublisher()
    }

    func getPlayedMatches(for teamId: String) -> AnyPublisher<MatchList, Error> {
        let request = URLRequest(url: NetworkingConfiguration.baseURL.appendingPathComponent("fixtures/team/\(teamId)/last/5"))

        return client.perform(request)
                .map(\.value)
                .eraseToAnyPublisher()
    }


}