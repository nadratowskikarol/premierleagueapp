import Foundation

class NetworkingConfiguration {
    static let baseURL = URL(string: "https://api-football-v1.p.rapidapi.com/v2/")!
    static let apiKey = ""
    static let apiHost = "api-football-v1.p.rapidapi.com"
}