import Foundation

class JSONFileMapper {
    static func map<T: Decodable>(fileName: String, outputType: T.Type) -> T? {
        guard let path = Bundle.main.path(forResource: "Resources/MockData/\(fileName)", ofType: "json") else {
            return nil
        }
        let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
        let decodedData = try? JSONDecoder().decode(outputType, from: data)
        return decodedData
    }
}