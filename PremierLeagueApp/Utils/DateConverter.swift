import Foundation

class DateConverter {
    static func convertDate(text: String) -> String {
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: text) ?? Date()
        dateFormatter.dateFormat = "MMM dd yyyy, HH:mm"
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate
    }
}
