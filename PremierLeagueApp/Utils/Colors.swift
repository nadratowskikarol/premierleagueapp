import UIKit

class Colors {
    static let mainColor: UIColor = UIColor(hexString: "#54a0ff")
    static let mainDarkColor: UIColor = UIColor(hexString: "#153184")
    static let red: UIColor = UIColor(hexString: "#ee5253")
}