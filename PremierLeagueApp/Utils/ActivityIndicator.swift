import UIKit
import SnapKit

class ActivityIndicator {
    static let shared = ActivityIndicator()

    private lazy var loaderImage: UIImageView = {
        let imageView = UIImageView()
        imageView.loadGif(name: "ball-loader")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var view: UIView = {
        let view = UIView()
        view.addSubview(loaderImage)
        view.backgroundColor = .white
        return view
    }()

    func start() {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else {
                return
            }

            let currentWindow: UIWindow? = UIApplication.shared.keyWindow
            currentWindow?.addSubview(self.view)
            self.view.fillParent()
            self.loaderImage.snp.makeConstraints {
                make in
                make.height.equalTo(150)
                make.center.equalToSuperview()
            }

        }
    }

    func stop() {
        DispatchQueue.main.async { [weak self] in
            self?.view.removeFromSuperview()
        }
    }
}
