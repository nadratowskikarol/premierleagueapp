import Quick
import Nimble
@testable import PremierLeagueApp

class TeamDetailsViewModelTests: QuickSpec {

    override func spec() {
        let team = Team(team_id: 1, name: "Testing Name", logo: "", venue_name: "Testing Stadium")
        let standing = Standing(team_id: 1, forme: "WWAD", points: 24, rank: 3)
        let teamWithStanding = TeamWithStanding(team: team, standings: standing)

        var spec = TeamDetailsViewModel(teamWithStanding: teamWithStanding)

        let matches = [
            Match(event_date: "", status: "status 1", homeTeam: MatchTeam(team_name: "asd", logo: "asd"),
                    awayTeam: MatchTeam(team_name: "asd", logo: "asd"), goalsHomeTeam: nil, goalsAwayTeam: nil, referee: ""),
            Match(event_date: "", status: "status 2", homeTeam: MatchTeam(team_name: "asd", logo: "asd"),
                    awayTeam: MatchTeam(team_name: "asd", logo: "asd"), goalsHomeTeam: nil, goalsAwayTeam: nil, referee: "")
        ]
        spec.matches = matches

        describe("TeamDetailsViewModel") {
            context("After being properly inizialized") {
                it("should have proper teamName") {
                    expect(spec.teamName).to(equal("Testing Name"))
                }

                it("should have proper stadiumName") {
                    expect(spec.stadiumName).to(equal("Stadium: Testing Stadium"))
                }

                it("should have proper number of Cells") {
                    expect(spec.numberOfCells).to(equal(2))
                }

                it("should have proper points") {
                    expect(spec.points).to(equal("24"))
                }

                it("should have proper rank") {
                    expect(spec.rank).to(equal("3rd"))
                }
            }

            context("getMatch()") {
                it("should return nil if index is out of range") {
                    expect(spec.getMatch(for: 2)).to(beNil())
                }

                it("should return proper match for valid index") {
                    let match = spec.getMatch(for: 1)!
                    expect(match.status).to(equal("status 2"))
                }
            }
        }
    }
}